package sb3.data;

import java.util.HashMap;
import java.util.Map;

import ast.Expr;
import ast.NumLiteral;
import ast.StrLiteral;
import ast.VarAccess;

public class Default {
	String fieldValue;

	Map<String, String> shadowAttrs;
	Map<String, String> fieldAttrs;

	public Default() {
		shadowAttrs = new HashMap<>();
		fieldAttrs = new HashMap<>();
	}

	public Default(String val) {
		this.fieldValue = val;
		shadowAttrs = new HashMap<>();
		fieldAttrs = new HashMap<>();
	}

	public Default(Map<String, String> shadowMeta, Map<String, String> fieldMeta, String fieldValue) {
		this.shadowAttrs = shadowMeta;
		this.fieldAttrs = fieldMeta;
		this.fieldValue = fieldValue;
	}

	public String getValue() {
		return getFieldValue();
	}

	public void setShadowAttr(Map<String, String> attrMap) {
		this.shadowAttrs = attrMap;
	}

	public void setShadowAttr(String key, String val) {
		this.shadowAttrs.put(key, val);
	}

	public Map<String, String> getShadowAttr() {
		return this.shadowAttrs;
	}

	public String getShadowAttr(String key) {
		return this.shadowAttrs.get(key);
	}

	public void setFieldAttr(String key, String val) {
		this.fieldAttrs.put(key, val);
	}

	public void setFieldAttr(Map<String, String> attrMap) {
		this.fieldAttrs = attrMap;
	}

	public Map<String, String> getFieldAttr() {
		return this.fieldAttrs;
	}

	public void setType(String type) {
		this.shadowAttrs.put("type", type);
	}

	public String getType() {
		return this.shadowAttrs.get("type");
	}

	public String getId() {
		return getShadowAttr("id");
	}

	public void setId(String id) {
		setShadowAttr("id", id);
	}

	public void setFieldName(String fieldName) {
		setFieldAttr("name", fieldName);
	}

	public String getFieldName() {
		return this.fieldAttrs.get("name");
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public Expr toExpr() {

		if (getType() == null) {
			StrLiteral res = new StrLiteral(getFieldValue());
			res.getAttrs().put("type", getType());
			return res;
		}
		if (getType().contains("math")) {
			NumLiteral res = new NumLiteral(getValue());
			res.getAttrs().put("type", getType());
			return res;
		}

		if (getType().contains("event_broadcast_menu")) {
			VarAccess varAcc = new VarAccess(getFieldAttr().get("id"));
			varAcc.setBlockId(getId()); // shadowId
			varAcc.setName(getFieldValue());
			varAcc.setType("broadcast_msg"); // set and used to properly convert to BlockEl
			return varAcc;
		}

		// default to literal node
		StrLiteral res = new StrLiteral(getValue());
		res.getAttrs().put("type", getType());
		

		return res;
	}

	@Override
	public String toString() {
		return "Default [fieldValue=" + fieldValue + ", shadowAttrs=" + shadowAttrs + ", fieldAttrs=" + fieldAttrs
				+ "]";
	}

	public Default(Default defaultVal) {
		this.shadowAttrs = new HashMap<String, String>(defaultVal.shadowAttrs);
		this.fieldAttrs = new HashMap<String, String>(defaultVal.fieldAttrs);
		this.fieldValue = new String(defaultVal.fieldValue);
	}

}
