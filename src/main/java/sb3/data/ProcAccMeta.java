package sb3.data;

import java.util.ArrayList;
import java.util.List;

public class ProcAccMeta {
	private String procCode;
	private List<String> argumentIds;

	public ProcAccMeta() {
		argumentIds = new ArrayList<String>();
	}

	public ProcAccMeta(ProcAccMeta meta) {
		procCode = meta.procCode;
		argumentIds = new ArrayList<>(meta.argumentIds);
	}

	public ProcAccMeta(String proccode, List<String> argumentids) {
		this.procCode = proccode;
		this.argumentIds = argumentids;
	}

	public List<String> getArgumentIds() {
		return argumentIds;
	}

	public String getProcCode() {
		return procCode;
	}

	public void setArgumentIds(List<String> argumentids) {
		this.argumentIds = argumentids;
	}

	public void setProccode(String proccode) {
		this.procCode = proccode;
	}

	public void setProcCode(String proccode) {
		this.procCode = proccode;
	}

	@Override
	public String toString() {
		return "ProcAccMeta [proccode=" + procCode + ", argumentids=" + argumentIds + "]";
	}

}
