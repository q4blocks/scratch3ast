package sb3.data;

public class ParamDeclMeta {
	public static class Builder {
		String name;
		String shadowId;
		String shadowType;
		String fieldName;
		String fieldValue;

		/**
		 * name should match argumentid
		 */
		public Builder(String name) {
			this.name = name;
		}

		public ParamDeclMeta build() {
			return new ParamDeclMeta(this);
		}

		public Builder fieldName(String name) {
			this.fieldName = name;
			return this;
		}

		public Builder fieldValue(String val) {
			this.fieldValue = val;
			return this;
		}

		public Builder shadowId(String id) {
			this.shadowId = id;
			return this;
		}

		public Builder shadowType(String type) {
			this.shadowType = type;
			return this;
		}
	}

	public static void main(String[] args) {
		ParamDeclMeta meta = new ParamDeclMeta.Builder("a").shadowId("shadid").shadowType("shadtype").fieldName("fname")
				.fieldValue("fval").build();
		System.out.println(meta);
	}

	private String name;
	private String shadowId;
	private String shadowType;
	private String fieldName;
	private String fieldValue;

	private ParamDeclMeta(Builder builder) {
		name = builder.name;
		shadowId = builder.shadowId;
		shadowType = builder.shadowType;
		fieldName = builder.fieldName;
		fieldValue = builder.fieldValue;
	}
	
	public ParamDeclMeta(ParamDeclMeta meta) {
		name = meta.name;
		shadowId = meta.shadowId;
		shadowType = meta.shadowType;
		fieldName = meta.fieldName;
		fieldValue = meta.fieldValue;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getFieldValue() {
		return fieldValue;
	}

	public String getName() {
		return name;
	}

	public String getShadowId() {
		return shadowId;
	}

	public String getShadowType() {
		return shadowType;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public void setFieldValue(String fieldValue) {
		this.fieldValue = fieldValue;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setShadowId(String shadowId) {
		this.shadowId = shadowId;
	}

	public void setShadowType(String shadowType) {
		this.shadowType = shadowType;
	}

	@Override
	public String toString() {
		return "ParamDeclMeta [name=" + name + ", shadowId=" + shadowId + ", shadowType=" + shadowType + ", fieldName="
				+ fieldName + ", fieldValue=" + fieldValue + "]";
	}

}
