package sb3.data;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Map;

import org.apache.commons.io.IOUtils;

import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class BlocksTemplate {

	public static Map<String, String> map = Maps.newHashMap();
	static {
		createMap();
	}

	public static void createMap() {
		try {
			String sb3TemplateJsonStr = readStringFromResource("./sb3opcode-template.json");
			Type type = new TypeToken<Map<String, String>>() {
			}.getType();
			Gson gson = new Gson();
			map = gson.fromJson(sb3TemplateJsonStr, type);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static String readStringFromResource(String fileName) throws IOException {
		InputStream inputStream = BlocksTemplate.class.getClassLoader().getResourceAsStream("sb3opcode-template.json");
		String theString = IOUtils.toString(inputStream);
		return theString;
	}

	public static String get(String opcode) {
		return map.get(opcode);
	}

}
