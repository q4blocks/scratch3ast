package sb3.data;

import java.util.ArrayList;
import java.util.List;

public class ProcDeclMeta {
	public static class Builder {
		private String shadowId;
		private String procCode;
		private List<String> argIds;
		private List<String> argNames;
		private List<Object> argDefaults;

		public Builder argDefaults(List<Object> defaults) {
			this.argDefaults = defaults;
			return this;
		}

		public Builder argIds(List<String> ids) {
			this.argIds = ids;
			return this;
		}

		public Builder argNames(List<String> names) {
			this.argNames = names;
			return this;
		}

		public ProcDeclMeta build() {
			return new ProcDeclMeta(this);
		}

		public Builder procCode(String code) {
			this.procCode = code;
			return this;
		}

		public Builder shadowId(String id) {
			this.shadowId = id;
			return this;
		}
	}

	public static void main(String[] args) {
		ProcDeclMeta meta = new ProcDeclMeta.Builder().shadowId("shadid").procCode("test %s %b")
				.argIds(new ArrayList<String>()).argNames(new ArrayList<String>()).argDefaults(new ArrayList<Object>())
				.build();
		System.out.println(meta);
	}

	private String shadowId;
	private String shadowType = "procedures_prototype";
	private String procCode;
	private List<String> argIds;
	private List<String> argNames;

	private List<Object> argDefaults;

	private boolean warp;

	private ProcDeclMeta(Builder builder) {
		shadowId = builder.shadowId;
		procCode = builder.procCode;
		argIds = builder.argIds;
		argNames = builder.argNames;
		argDefaults = builder.argDefaults;
		warp = false;
	}

	public ProcDeclMeta(ProcDeclMeta meta) {
		shadowId = meta.shadowId;
		procCode = meta.procCode;
		argIds = new ArrayList<>(meta.argIds);
		argNames = new ArrayList<>(meta.argNames);
		argDefaults = new ArrayList<>(meta.argDefaults);
		warp = meta.warp;
	}

	public List<Object> getArgDefaults() {
		return argDefaults;
	}

	public List<String> getArgIds() {
		return argIds;
	}

	public List<String> getArgNames() {
		return argNames;
	}

	public String getProcCode() {
		return procCode;
	}

	public String getShadowId() {
		return shadowId;
	}

	public String getShadowType() {
		return shadowType;
	}

	public boolean isWarp() {
		return warp;
	}

	public void setArgDefaults(List<Object> argDefaults) {
		this.argDefaults = argDefaults;
	}

	public void setArgIds(List<String> argIds) {
		this.argIds = argIds;
	}

	public void setArgNames(List<String> argNames) {
		this.argNames = argNames;
	}

	public void setProcCode(String procCode) {
		this.procCode = procCode;
	}

	public void setShadowId(String shadowId) {
		this.shadowId = shadowId;
	}

	public void setShadowType(String shadowType) {
		this.shadowType = shadowType;
	}

	public void setWarp(boolean warp) {
		this.warp = warp;
	}

	@Override
	public String toString() {
		return "ProcDeclMeta [shadowId=" + shadowId + ", shadowType=" + shadowType + ", procCode=" + procCode
				+ ", argIds=" + argIds + ", argNames=" + argNames + ", argDefaults=" + argDefaults + ", warp=" + warp
				+ "]";
	}
}
