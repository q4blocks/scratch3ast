package sb3.data;

import java.util.Set;

import com.google.common.collect.Sets;

public class BlocksInfo {
	public static final Set<String> invalidBlockInputs = Sets.newHashSet(
			"motion_setrotationstyle",
			"event_whenkeypressed",
			"looks_changeeffectby",
			"looks_seteffectto",
			"looks_gotofrontback",
			"looks_goforwardbackwardlayers",
			"looks_costumenumbername",
			"looks_backdropnumbername",
			"sound_changeeffectby",
			"sound_seteffectto",
			"event_whenkeypressed",
			"event_whenbackdropswitchesto",
			"event_whengreaterthan",
			"event_whenbroadcastreceived",
			"control_stop",
			"sensing_touchingobject",
			"sensing_keyoptions",
			"sensing_setdragmode",
			"sensing_of",
			"sensing_current",
			"operator_mathop",
			"data_setvariableto",
			"data_changevariableby",
			"data_showvariable",
			"data_hidevariable");
	public static boolean isReplaceable(String opcode) {
		return !invalidBlockInputs.contains(opcode);
	}
}
