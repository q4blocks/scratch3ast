package sb3.data;

import java.util.ArrayList;
import java.util.List;

import ast.Expr;
import ast.ExprStmt;
import ast.OperatorExpr;
import ast.ProcAccess;

public class BlockArgHelper {
	public static void setArg(ExprStmt block, Expr arg, int idx) {
		block.setArg(arg, idx);
	}

	public static void setArg(OperatorExpr block, Expr arg, int idx) {
		block.setArg(arg, idx);
	}

	public static void setArg(ProcAccess block, Expr arg, int idx) {
		block.setArg(arg, idx);
	}

	public static void addArg(ExprStmt block, Expr arg) {
		block.addArg(arg);
	}

	public static void addArg(OperatorExpr block, Expr arg) {
		block.addArg(arg);
	}

	public static void addArg(ProcAccess block, Expr arg) {
		block.addArg(arg);
	}

	public static java.util.List<Expr> getArgInputs(OperatorExpr block) {
		List<Expr> args = new ArrayList<>();
		for (Expr arg : block.getArgs()) {
			args.add(arg);
		}
		return args;
	}

	public static java.util.List<Expr> getArgInputs(ExprStmt block) {
		List<Expr> args = new ArrayList<>();
		for (Expr arg : block.getArgs()) {
			args.add(arg);
		}
		return args;
	}

	public static java.util.List<Expr> getArgInputs(ProcAccess block) {
		List<Expr> args = new ArrayList<>();
		for (Expr arg : block.getArgs()) {
			args.add(arg);
		}
		return args;
	}
}
