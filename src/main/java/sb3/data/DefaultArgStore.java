package sb3.data;

import java.util.Arrays;

public class DefaultArgStore {
	Default[] defaults = new Default[0];

	public DefaultArgStore() {

	}

	public void initDefault(int size) {
		this.defaults = new Default[size];
	}

	public void setDefault(Default defaultVal, int idx) {
		this.defaults[idx] = defaultVal;
	}

	public Default[] getDefaults() {
		return defaults;
	}

	public Default getDefault(int idx) {
		if (idx > defaults.length - 1) {
			return null;
		}
		return this.defaults[idx];
	}

	public DefaultArgStore(DefaultArgStore argStore) {
		this.defaults = new Default[argStore.getDefaults().length];
		for (int i = 0; i < argStore.getDefaults().length; i++) {
			if (argStore.getDefault(i) != null) {
				defaults[i] = new Default(argStore.getDefault(i));
			}
		}
	}

	@Override
	public String toString() {
		return "[defaults=" + Arrays.toString(defaults) + "]";
	}
}
