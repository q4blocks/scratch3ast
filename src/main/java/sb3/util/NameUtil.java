package sb3.util;

public class NameUtil {
	public static String shorten(String inputString) {
		if(inputString==null) {
			return "";
		}
		int MAX_CHAR = 3;
		int maxLength = (inputString.length() < MAX_CHAR)?inputString.length():MAX_CHAR;
		String shortString = inputString.substring(0, maxLength);
		return shortString;
	}
	
	public static String shortenType(String blockType) {
		String[] res = blockType.split("_");
		if(res.length>1) {
			return res[1];			
		}else {
			return blockType;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(shortenType("operator_multiply"));
	}
}
