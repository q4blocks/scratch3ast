package sb3.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.FileUtils;

public class Graphviz {

	public void generateGraph(final File subTestDirEntry) throws IOException {
		Process process = new ProcessBuilder("/usr/bin/dot", "-Tpng", "-o",
				getFilePathInDir(subTestDirEntry, "output.png"), getFilePathInDir(subTestDirEntry, "out.dot")).start();
	}

	/**
	 * 
	 * @param graphSpec
	 * @param outputName
	 *            (without .dot)
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public void generateGraph(String graphSpec, String outputName) throws IOException, InterruptedException {
		File file = new File(outputName + ".dot");
		String encoding = null;
		FileUtils.writeStringToFile(file, graphSpec, encoding);
		Process process = new ProcessBuilder("/usr/bin/dot", "-Tpng", "-o", outputName + ".png", outputName + ".dot")
				.start();
		int errCode = process.waitFor();
		if (errCode != 0) {
			System.err.println("Echo Output:\n" + output(process.getInputStream()));
		}
		FileUtils.deleteQuietly(file);
	}

	private String getFilePathInDir(File subDirEntry, String filename) throws IOException {
		File file = new File(subDirEntry, filename);
		return file.toPath().toString();
	}

	private static String output(InputStream inputStream) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;
			while ((line = br.readLine()) != null) {
				sb.append(line + System.getProperty("line.separator"));
			}
		} finally {
			br.close();
		}
		return sb.toString();
	}

}
