package sb3.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.util.Arrays;
import java.util.Optional;

import ast.ASTNodeAnnotation;

public class Util {

	private static String getChildNameHelper(Member member, int j) {
		Constructor c = (Constructor) member;
		Annotation[] types = c.getDeclaredAnnotations();
		ASTNodeAnnotation.Constructor constAnnotation = c.getDeclaredAnnotation(ASTNodeAnnotation.Constructor.class);
		return constAnnotation.name()[j];
	}

	public static String getChildName(Class c, int pos) throws Exception {
		Constructor[] constructors = c.getDeclaredConstructors();
		Optional<Constructor> annotatedConstructor = Arrays.stream(constructors).filter(cons -> cons.getDeclaredAnnotation(ASTNodeAnnotation.Constructor.class)!=null).findFirst();
		String name = "";
		if(c!=ast.List.class && annotatedConstructor.isPresent()) {
			name = getChildNameHelper(annotatedConstructor.get(), pos);
		}else {
			throw new Exception("Unable to identify the name of the null child node\n No constructor annotation found for "+c.getSimpleName());
		}
		return name;
	}
}
