package sb3.main;

import org.junit.Test;

import ast.BroadcastStmt;

public class PrintTest {

	@Test(expected = NullPointerException.class)
	public void test() throws Exception {
		BroadcastStmt broadcatstmt = new BroadcastStmt();
		broadcatstmt.setBlockId("id1");
		System.out.println(broadcatstmt.print());
	}
}
